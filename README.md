## Application Details
|               |
| ------------- |
|**Generation Date and Time**<br>Wed Mar 24 2021 14:52:34 GMT+0000 (Coordinated Universal Time)|
|**App Generator**<br>@sap/generator-fiori|
|**App Generator Version**<br>1.1.5|
|**Generation Platform**<br>SAP Business Application Studio|
|**Floorplan Used**<br>List Report Object Page V2|
|**Service Type**<br>SAP System (ABAP On Premise)|
|**Service URL**<br>http://fiorides.alcsa.com.gt:44300/sap/opu/odata/sap/ZCDSFI_REP_DIARIO_MAYOR_CDS/
|**Module Name**<br>project1|
|**Application Title**<br>App Title|
|**Namespace**<br>|
|**UI5 Theme**<br>sap_fiori_3|
|**UI5 Version**<br>Latest|
|**Enable Telemetry**<br>True|
|**Main Entity**<br>ZCDSFI_REP_DIARIO_MAYOR|
|**Navigation Entity**<br>to_cta|

## project1

A Fiori application.

### Starting the generated app

-   This app has been generated using the SAP Fiori tools - App Generator, as part of the SAP Fiori tools suite.  In order to launch the generated app, simply run the following from the generated app root folder:

```
    npm start
```


#### Pre-requisites:

1. Active NodeJS LTS (Long Term Support) version and associated supported NPM version.  (See https://nodejs.org)


